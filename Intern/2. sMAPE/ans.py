import numpy as np


def smape(y_true: np.array, y_pred: np.array) -> float:
    if np.linalg.matrix_rank(y_true) == np.linalg.matrix_rank(y_pred) == 0:
        return 0
    return np.mean(2 * np.abs(y_true - y_pred) / (np.abs(y_true) + np.abs(y_pred)))