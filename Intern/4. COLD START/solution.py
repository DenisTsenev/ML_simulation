import pandas as pd


def fillna_with_mean(
    df: pd.DataFrame, target: str, group: str
) -> pd.DataFrame:
    
    df[target] = df[target].apply(lambda x: df.groupby(group)[target].mean() if x == None else x)
    
    return df
